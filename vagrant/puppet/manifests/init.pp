exec { "apt-update":
  command => "/usr/bin/apt-get update"
}
Exec["apt-update"] -> Package <| |>

#include redis

package { 'php5-fpm':
  ensure => present,
}

package { 'php5-cli':
  ensure => present,
}

package { 'nginx':
  ensure => present,
}


package { 'vim':
  ensure => present,
}

service { 'nginx':
  ensure => running,
         require => Package['nginx'],
}

service { 'php5-fpm':
  ensure => running,
         require => Package['php5-fpm'],
}

file { '/etc/nginx/sites-enabled/default':
  source => '/vagrant/vagrant/puppet/config/nginx.default.conf',
    owner => 'root',
    group => 'root',
    mode => '640',
    notify => Service['nginx'],
    require => Package['nginx'],
}

class { '::mysql::server':
  root_password    => 'pass',
    override_options => { 'mysqld' => { 'max_connections' => '1024' } }
}

class { '::mysql::bindings':
    php_enable => true;
}

mysql_database { 'kripra':
  ensure  => 'present',
  charset => 'utf8',
  collate => 'utf8_general_ci',
}

exec { 'migrate db':
    command => '/usr/bin/php /vagrant/kripra/protected/yiic.php migrate --interactive=0',
    path => '/vagrant/kripra/protected/',
    require => [
      Service['mysqld'],
      Mysql_Database['kripra'],
    ]
}

mysql_grant { 'root@127.0.0.1/*.*':
  ensure     => 'present',
  options    => ['GRANT'],
  privileges => ['ALL'],
  table      => 'kripra.*',
  user       => 'root@127.0.0.1',
}