class { 'nginx': }

nginx::resource::vhost { 'kripera.dev':
  ensure   => present,
    www_root => '/vagrant/kripra',
}
