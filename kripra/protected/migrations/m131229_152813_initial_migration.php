<?php

class m131229_152813_initial_migration extends CDbMigration
{
	public function up()
	{
                    $this->execute("CREATE TABLE IF NOT EXISTS `currency` (
                                            `name` varchar(20) NOT NULL,
                                            `label` varchar(100) NOT NULL,
                                            PRIMARY KEY (`name`)
                                          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
                        $this->execute("CREATE TABLE IF NOT EXISTS `user` (
                                            `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                            `first_name` varchar(40) NOT NULL,
                                            `last_name` varchar(40) NOT NULL,
                                            `email` varchar(100) NOT NULL,
                                            `password_hash` varchar(64) NOT NULL,
                                            `verification_code` varchar(10) NOT NULL,
                                            `verified` tinyint(1) NOT NULL DEFAULT '0',
                                            PRIMARY KEY (`id`),
                                            UNIQUE KEY `email` (`email`),
                                            KEY `email_2` (`email`)
                                          ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");
                        $this->execute("CREATE TABLE IF NOT EXISTS `wallet` (
                                            `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                            `user` bigint(20) NOT NULL,
                                            `currency` varchar(20) NOT NULL,
                                            `balance` decimal(20,10) NOT NULL,
                                            PRIMARY KEY (`id`),
                                            UNIQUE KEY `user_2` (`user`,`currency`),
                                            KEY `currency` (`currency`)
                                          ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");
                            $this->execute("CREATE TABLE IF NOT EXISTS `address` (
                                            `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                            `address` varchar(250) NOT NULL,
                                            `wallet` bigint(20) DEFAULT NULL,
                                            `currency` varchar(20) NOT NULL,
                                            PRIMARY KEY (`id`),
                                            KEY `wallet` (`wallet`),
                                            KEY `address` (`address`),
                                            KEY `currency` (`currency`)
                                          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
                            $this->execute("CREATE TABLE IF NOT EXISTS `history` (
                                            `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                            `type` enum('buy','sell','withdraw','addfunds') NOT NULL,
                                            `created` datetime NOT NULL,
                                            `user` bigint(20) NOT NULL,
                                            PRIMARY KEY (`id`),
                                            KEY `user` (`user`)
                                          ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");

                        $this->execute("CREATE TABLE IF NOT EXISTS `history_amount` (
                                            `id` bigint(20) NOT NULL AUTO_INCREMENT,
                                            `history` bigint(20) NOT NULL,
                                            `currency` varchar(20) NOT NULL,
                                            `amount` decimal(20,10) NOT NULL,
                                            PRIMARY KEY (`id`),
                                            KEY `history_id` (`currency`),
                                            KEY `currency` (`currency`),
                                            KEY `history` (`history`)
                                          ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");


                            
                            $this->execute("ALTER TABLE `address`
                                            ADD CONSTRAINT `address_ibfk_1` FOREIGN KEY (`wallet`) REFERENCES `wallet` (`id`),
                                            ADD CONSTRAINT `address_ibfk_2` FOREIGN KEY (`currency`) REFERENCES `currency` (`name`);
                                          ");
                            $this->execute("ALTER TABLE `history`
                                            ADD CONSTRAINT `history_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
                                          ");

                            $this->execute("ALTER TABLE `history_amount`
                                            ADD CONSTRAINT `history_amount_ibfk_2` FOREIGN KEY (`currency`) REFERENCES `currency` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
                                            ADD CONSTRAINT `history_amount_ibfk_3` FOREIGN KEY (`history`) REFERENCES `history` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");

                            $this->execute("ALTER TABLE `wallet`
                                            ADD CONSTRAINT `wallet_ibfk_3` FOREIGN KEY (`user`) REFERENCES `user` (`id`),
                                            ADD CONSTRAINT `wallet_ibfk_4` FOREIGN KEY (`currency`) REFERENCES `currency` (`name`);");

                            $this->execute("DROP TRIGGER IF EXISTS `delete_empty_wallets_`;");
                            $this->execute("CREATE TRIGGER `delete_empty_wallets_` BEFORE DELETE ON `currency`
                                             FOR EACH ROW DELETE FROM wallet where currency = OLD.name AND balance=0;");
                            $this->execute("DROP TRIGGER IF EXISTS `delete_empty_wallets`;");
                            $this->execute("CREATE TRIGGER `delete_empty_wallets` BEFORE DELETE ON `user`
                                             FOR EACH ROW DELETE FROM wallet where user = OLD.id AND amount=0;");
	}

	public function down()
	{
		echo "m131229_152813_initial_migration does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}