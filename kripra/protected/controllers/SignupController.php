<?php


class SignupController extends Controller {
    
    public function actionIndex() {
        
        $model = new User('signup');


        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $valid = $model->validate();
            if ($valid) {
                if ($model->save()) {
                    $mail = new YiiMailer();
                    $mail->setView('signupSuccess');
                    $mail->setData(array( 'link'=>$this->createAbsoluteUrl("signup/verifyUser",array('email'=>$model->email,'verificationCode'=>$model->verification_code))));
                    $mail->setFrom(Yii::app()->params['supportEmail'], Yii::app()->name);
                    $mail->setTo($model->email);
                    $mail->setSubject('Registiration successfull');
                    
                    if ($mail->send()) {
                        Yii::app()->user->setFlash('contact', Yii::t('app','Please check your e-mail to complete your sign-up process'));
                    } else {
                        Yii::app()->user->setFlash('error', Yii::t('app','Error while sending email: {email}', array('{email}'=>$mail->getError())) );
                    }
                    
                    $this->redirect(array('signup/success'));
                }
            } else {
                $error = CActiveForm::validate($model);
            }
        }
        $this->render('index', array('model' => $model));
    }

    public function actionSuccess() {
         $this->render('success');
    }
    
    
    public function actionVerifyUser($email,$verificationCode) {
        $result = false;
        $model = User::model()->findByAttributes(array("email"=>$email));
        
        if($model && $model->verification_code = $verificationCode) {
            $model->verified = true;
            if($model->save()){
               $result = true;
            }
        }
        if($result) {
            Yii::app()->user->setFlash('success', ii::t('app','Please login'));
            $this->redirect(array('login/index'));
              
        }  else {
            Yii::app()->user->setFlash('error', ii::t('app','Cannot verify your account'));
            $this->redirect(array('main/index'));
        }
        
         
    }
    
}

?>