<?php

class LoginController extends Controller {

    public function actionIndex() {
        $model = new LoginForm;

        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            $valid = $model->validate();
            if ($valid) {
                if ($model->login()) {
                    Yii::app()->user->setFlash('success', Yii::t('app','Login successful'));
                    $this->redirect(array('wallet/index'));
                } else {
                    $error = array(Yii::t('app','Cannot login'));
                }
            } else {
                $error = CActiveForm::validate($model);
            }
        }

        $this->render('index', array('model' => $model));
    }

}
?>