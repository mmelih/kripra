<?php

Yii::import('application.controllers.AccountController');

class WalletController extends AccountController {

    private $_wallet;
    private $_currency_action;

    public function beforeAction($action) {
        $this->_user->createAllWallets();

        $currency = Yii::app()->getRequest()->getQuery('currency');


        if (!empty($currency)) {
            $wallets = $this->_user->wallets;

            foreach ($wallets as $w) {
                if ($w->currency == $currency) {
                    $this->_wallet = $w;
                }
            }
        }

        if (!empty($this->_wallet) && empty($this->_currency_action)) {
            $this->_currency_action = CurrencyAction::getAction($this->_wallet->currency);
        }

        return parent::beforeAction($action);
    }

    public function actionIndex() {

        $history = History::model()->with('history_amount')->findAllByAttributes(array('user'=>$this->_user->id));
        $this->render('index', array('wallets' => $this->_user->wallets, 'history' => $history));
    }

    public function actionMain() {
        $history = History::model()->with(array('history_amount' => array('joinType' => 'INNER JOIN',
                    'condition' => 'history_amount.currency=:currency',
                    'params' => array(':currency' => $this->_wallet->currency))))->findAll();

        $this->render('main', array('wallet' => $this->_wallet, 'history' => $history));
    }

    public function actionAddfunds() {
        $result = $this->_currency_action->addFunds($this->_wallet, $form_model);

        if ($result) {

            $history = new History();
            $history->type = 'addfunds';
            $history->user = $this->_user->id;
            if ($history->save()) {
                $historyAmount = new HistoryAmount;
                $historyAmount->currency = $this->_wallet->currency;
                $historyAmount->amount = $form_model->amount;
                $historyAmount->history = $history->id;
                if (!$historyAmount->save()) {
                    Yii::log("Cannot save addfunds history amount", "error");
                }
            }

            Yii::app()->user->setFlash('success', Yii::t('app', 'Addfunds successfull'));
            $this->redirect(Yii::app()->createUrl("wallet/main", array('currency' => $this->_wallet->currency)));
            Yii::app()->end();
        }

        $this->render($this->_currency_action->getAddFundsView(), array('wallet' => $this->_wallet,
            'data' => $result,
            'model' => $form_model));
    }

    public function actionWithdraw() {
        $result = $this->_currency_action->withdraw($this->_wallet, $form_model);
        if ($result) {

            $history = new History();
            $history->type = 'withdraw';
            $history->user = $this->_user->id;
            if ($history->save()) {
                $historyAmount = new HistoryAmount;
                $historyAmount->currency = $this->_wallet->currency;
                $historyAmount->amount = $form_model->amount;
                $historyAmount->history = $history->id;
                if (!$historyAmount->save()) {
                    Yii::log("Cannot save withdraw history amount", "error");
                }
            }

            Yii::app()->user->setFlash('success', Yii::t('app', 'Witdraw successfull'));
            $this->redirect(Yii::app()->createUrl("wallet/main", array('currency' => $this->_wallet->currency)));
            Yii::app()->end();
        }

        $this->render($this->_currency_action->getWithdrawView(), array('wallet' => $this->_wallet,
            'data' => $result,
            'model' => $form_model));
    }

}

?>