<?php

class AccountController extends SecureController {

    protected $_user;
    
    public function __construct($id, $module = null) {
        $this->_user = User::model()->findByPk(Yii::app()->user->id);
        if(empty($this->_user)){
            return false;
        } 
        return parent::__construct($id, $module);
    }
    
    public function beforeAction($action) {
        return parent::beforeAction($action);
    }
    
}

?>