<?php

class User extends CActiveRecord {

    public $password;
    public $password_repeat;

    public function rules() {
        return array(
            array('email,password, password_repeat', 'required', 'on' => 'signup'),
            array('password, password_repeat', 'length', 'min' => 8, 'max' => 40, 'on' => 'signup'),
            array('password', 'compare', 'compareAttribute' => 'password_repeat', 'on' => 'signup'),
            array('email,password', 'required', 'on' => 'login'),
            array('email', 'email'),
            array('email','unique'),
        );
    }

    public function attributeLabels() {
        return array(
            'email' => Yii::t('app','E-mail'),
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'user';
    }

    public function relations()
    {
        return array(
            'wallets'=>array(self::HAS_MANY, 'Wallet', 'user'),
        );
    }
    public function validatePassword($password) {
        return CPasswordHelper::verifyPassword($password, $this->password_hash);
    }

    public function hashPassword($password) {
        return CPasswordHelper::hashPassword($password);
    }

    public function beforeSave() {
        if (!empty($this->password) && !empty($this->password_repeat)) {
            $this->password_hash = $this->hashPassword($this->password);
        }

        if ($this->isNewRecord) {
            $this->setVerificationCode();
        }

        return parent::beforeSave();
    }

    public function setVerificationCode() {
        $this->verification_code = Yii::app()->getSecurityManager()->generateRandomString(10);
    }
    
    public function createAllWallets() {
        $missingWalletCurrencies = Yii::app()->db->createCommand('SELECT c.name FROM currency c 
                                                LEFT OUTER JOIN wallet w ON w.currency=c.name AND w.user = :user
                                                    WHERE w.currency IS NULL ')->bindValue('user',$this->id)->queryAll();
        foreach($missingWalletCurrencies as $c) {
            $newWallet = new Wallet;
            $newWallet->attributes = array('user'=>$this->id,'currency'=>$c['name']);
            $newWallet->save();
        }
    }

}

?>
