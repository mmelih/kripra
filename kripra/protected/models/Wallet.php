<?php

class Wallet extends CActiveRecord {


    public function rules() {
        return array(
            array('user,currency', 'required'),
            array('balance', 'numerical', 'min'=>0)
        );
    }

    public function relations()
    {
        return array(
            'user'=>array(self::BELONGS_TO, 'User', 'id'),
            'currency'=>array(self::BELONGS_TO, 'Currency','name'),
        );
    }
    

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'wallet';
    }

}


?>