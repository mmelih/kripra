<?php

class History extends CActiveRecord {

    public function relations() {
        return array(
            'history_amount' => array(self::HAS_MANY, 'HistoryAmount', 'history'),
            'user' => array(self::BELONGS_TO, 'User', 'id'),
        );
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'history';
    }

    public function beforeSave() {
        if ($this->isNewRecord) {
            $this->created = new CDbExpression('NOW()');
        }
        return parent::beforeSave();
    }

}

?>