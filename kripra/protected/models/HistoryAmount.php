<?php

class HistoryAmount extends CActiveRecord {


    public function relations()
    {
        return array(
            'history'=>array(self::BELONGS_TO, 'History', 'id'),
            'currency'=>array(self::BELONGS_TO, 'Currency', 'name'),
        );
    }
    

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'history_amount';
    }


}

?>