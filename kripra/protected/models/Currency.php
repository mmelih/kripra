<?php

class Currency extends CActiveRecord {


    public function rules() {
        return array(
            array('name,label,class', 'required'),
        );
    }

    public function relations()
    {
        return array(
            'wallet'=>array(self::HAS_MANY, 'Wallet', 'currency')
        );
    }
    

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'currency';
    }


}

?>