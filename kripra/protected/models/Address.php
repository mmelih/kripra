<?php 

class Address extends CActiveRecord {


    public function rules() {
        return array(
            array('address,currency', 'required'),
        );
    }

    public function relations()
    {
        return array(
            'wallet'=>array(self::BELONGS_TO, 'Wallet', 'id'),
            'currency'=>array(self::BELONGS_TO, 'Currency', 'name'),
        );
    }
    

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'address';
    }

}

?>