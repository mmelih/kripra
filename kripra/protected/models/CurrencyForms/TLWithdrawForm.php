<?php

class TLWithdrawForm extends CFormModel {

    public $amount;

    public function rules() {
        return array(
            array('amount', 'required'),
            array('amount','numerical', 'min'=>1),
        );
    }
}
?>
