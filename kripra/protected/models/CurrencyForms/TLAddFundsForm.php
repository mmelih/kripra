<?php

class TLAddFundsForm extends CFormModel {

    public $amount;

    public function rules() {
        return array(
            array('amount', 'required'),
            array('amount','numerical', 'min'=>1),
        );
    }
}
?>
