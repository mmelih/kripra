<?php

class LoginForm extends CFormModel {

    public $email;
    public $password;
    private $_identity;

    public function rules() {
        return array(
            array('email, password', 'required'),
            array('email', 'email'),
        );
    }

    public function authenticate($attribute, $params) {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->email, $this->password);
            return $this->_identity->authenticate();
        }
        
        return false;
    }

    public function login() {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->email, $this->password);
            $this->_identity->authenticate();
        }
        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            Yii::app()->user->login($this->_identity, 0);
            return true;
        } else {
            $this->addError('password', ii::t('app','Incorrect e-mail or password.'));
            return false;
        }
    }

}
