<div class="form">
<?php echo CHtml::beginForm('login'); ?>
 
    <?php echo CHtml::errorSummary($model); ?>
 
    <div class="row">
        <?php echo CHtml::activeLabel($model,'email'); ?>
        <?php echo CHtml::activeTextField($model,'email') ?>
    </div>
 
    <div class="row">
        <?php echo CHtml::activeLabel($model,'password'); ?>
        <?php echo CHtml::activePasswordField($model,'password') ?>
    </div>
 
    <div class="row submit">
        <?php echo CHtml::submitButton(Yii::t('app','Login')); ?>
    </div>
 
<?php echo CHtml::endForm(); ?>