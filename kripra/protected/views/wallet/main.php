<h1><?php echo $wallet->currency; ?> <?php echo $wallet->balance; ?></h1>

<div style="float:left;margin-right:auto">

<p><a href="<?php echo Yii::app()->createUrl("wallet/addFunds", array('currency' => $wallet->currency)); ?>">Add Funds</a> 
</p>
<p><a href="<?php echo Yii::app()->createUrl("wallet/withdraw", array('currency' => $wallet->currency)); ?>">Withdraw</a> 
</p>
</div>
<div style="float:right;margin-left:auto">

    <?php $this->renderPartial('transaction_history', array('history'=>$history)); ?>
</div>