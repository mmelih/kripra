
<div style="float:left;margin-right:auto">
    <ul>
        <?php
        foreach ($wallets as $w) {
            ?>
            <li>
                <a href="<?php echo Yii::app()->createUrl("wallet/main", array('currency' => $w->currency)); ?>">
                    <?php
                    echo $w->currency . ':' . $w->balance;
                    ?>
                </a>
            </li>
            <?php
        }
        ?>
    </ul>
</div>
<div style="float:right;margin-left:auto">
    <?php $this->renderPartial('transaction_history', array('history' => $history)); ?>
</div>

