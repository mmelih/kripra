<div class="form">
<?php echo CHtml::beginForm('withdraw'); ?>
 
    <?php echo CHtml::errorSummary($model); ?>
    <div class="row">
        <?php echo CHtml::activeLabel($model,'amount'); ?>
        <?php echo CHtml::activeTextField($model,'amount') ?>
    </div>
 
    <div class="row submit">
        <?php echo CHtml::submitButton(Yii::t('app','Withdraw')); ?>
    </div>
 
<?php echo CHtml::endForm(); ?>