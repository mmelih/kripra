<div class="form">
<?php echo CHtml::beginForm('addFunds'); ?>
 
    <?php echo CHtml::errorSummary($model); ?>
    <div class="row">
        <?php echo CHtml::activeLabel($model,'amount'); ?>
        <?php echo CHtml::activeTextField($model,'amount') ?>
    </div>
 
    <div class="row submit">
        <?php echo CHtml::submitButton(Yii::t('app','Addfunds')); ?>
    </div>
 
<?php echo CHtml::endForm(); ?>