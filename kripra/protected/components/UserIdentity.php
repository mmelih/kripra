<?php

class UserIdentity extends CUserIdentity {
    private $_id;
    public function authenticate() {
        $user = User::model()->findByAttributes(array("email"=>$this->username));
        if ($user) {
            if ($user->validatePassword($this->password)) {
                $this->errorCode = self::ERROR_NONE;
                $this->_id = $user->id;
            } else {
                $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
            }
        } else {
            $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
        }

        return $this->errorCode;
    }
    
    public function getId()
    {
        return $this->_id;
    }

}