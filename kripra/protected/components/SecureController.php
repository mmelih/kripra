<?php

class SecureController extends Controller {

    public function beforeAction($action) {
        if(Yii::app()->user->isGuest) {
            // TODO: redirect user to login page
            return false;
        }
        return parent::beforeAction($action);
    }
}

?>