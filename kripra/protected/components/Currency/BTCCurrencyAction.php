<?php

class BTCCurrencyAction extends CurrencyAction {

    public function __construct() {
        parent::__construct('BTC');
    }

    public function addFunds(&$wallet,&$form_model) {
        $form_model = new BTCAddFundsForm();

        if (isset($_POST['BTCAddFundsForm'])) {
            $form_model->attributes = $_POST['BTCAddFundsForm'];
            if ($form_model->validate()) {
                $wallet->balance += $form_model->amount;
                if($wallet->save()) {
                    return true;
                }
            }
        }

        return false;
    }

    public function withdraw(&$wallet,&$form_model) {
        $form_model = new BTCWithdrawForm();

        if (isset($_POST['BTCWithdrawForm'])) {
            $form_model->attributes = $_POST['BTCWithdrawForm'];
            if ($form_model->validate()) {
                $wallet->balance -= $form_model->amount;
                if($wallet->save()) {
                    return true;
                } else {
                    $form_model->addError("amount", "Your balance is not enough");
                }
            }
        }

        return false;
    }

}

?>
