<?php


abstract class CurrencyAction {
    protected $_currency;
    public function __construct($currency_name) {
        $this->_currency=Currency::model()->find("name=:currency_name", array(':currency_name'=>$currency_name));;
    }
    
    public static function getAction($currency_name) {
        $className = $currency_name.'CurrencyAction'; 
        return new $className;
    }
    
    public function getAddFundsView(){
        return 'currency/'.($this->_currency->name).'/add_funds';
    }
    
    public function getWithdrawView(){
        return 'currency/'.($this->_currency->name).'/withdraw';
    }
    
    public function getAddFundsModel(){
        $className = $this->_currency->name.'AddFundsForm';
        return new $className;
    }
    
    public function getWithdrawModel(){
        $className = $this->_currency->name.'WithdrawForm';
        return new $className;
    }
    
    public abstract function addFunds(&$wallet,&$form_model);
    public abstract function withdraw(&$wallet,&$form_model);
    
    
}
?>
