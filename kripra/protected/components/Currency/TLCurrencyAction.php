<?php

class TLCurrencyAction extends CurrencyAction {

    public function __construct() {
        parent::__construct('TL');
    }

    public function addFunds(&$wallet,&$form_model) {
        $form_model = new TLAddFundsForm();

        if (isset($_POST['TLAddFundsForm'])) {
            $form_model->attributes = $_POST['TLAddFundsForm'];
            if ($form_model->validate()) {
                $wallet->balance += $form_model->amount;
                if($wallet->save()) {
                    return true;
                }
            }
        }

        return false;
    }

    public function withdraw(&$wallet,&$form_model) {
        $form_model = new TLWithdrawForm();

        if (isset($_POST['TLWithdrawForm'])) {
            $form_model->attributes = $_POST['TLWithdrawForm'];
            if ($form_model->validate()) {
                $wallet->balance -= $form_model->amount;
                if($wallet->save()) {
                    return true;
                } else {
                    $form_model->addError("amount", "Your balance is not enough");
                }
            }
        }

        return false;
    }

}

?>
