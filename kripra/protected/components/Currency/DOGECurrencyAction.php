<?php

class DOGECurrencyAction extends CurrencyAction {

    public function __construct() {
        parent::__construct('DOGE');
    }

    public function addFunds(&$wallet,&$form_model) {
        $form_model = new DOGEAddFundsForm();

        if (isset($_POST['DOGEAddFundsForm'])) {
            $form_model->attributes = $_POST['DOGEAddFundsForm'];
            if ($form_model->validate()) {
                $wallet->balance += $form_model->amount;
                if($wallet->save()) {
                    return true;
                }
            }
        }

        return false;
    }

    public function withdraw(&$wallet,&$form_model) {
        $form_model = new DOGEWithdrawForm();

        if (isset($_POST['DOGEWithdrawForm'])) {
            $form_model->attributes = $_POST['DOGEWithdrawForm'];
            if ($form_model->validate()) {
                $wallet->balance -= $form_model->amount;
                if($wallet->save()) {
                    return true;
                } else {
                    $form_model->addError("amount", "Your balance is not enough");
                }
            }
        }

        return false;
    }

}

?>
