<?php

/*
 * Position definitions
 */

define('P_FRONTEND', 0);
define('P_BACKEND', 1);


/*
 * Job Definitions
 */

define('J_NEW_ADDRESS_REQUEST',000);


/*
 * Other definition
 */
define('MIN_NUM_OF_AVAILABLE_ADDRESSES', 50);


abstract class KWorker {

    private $_position;
    private $_currency;

    public function __construct($position) {
        $this->_position = $position;
        $this->_mainThread();
    }

    /*
     * This is called when a job received from backend
     */

    protected abstract function _processJobFromBackend(KJob $job);

    /*
     * This is called when a job received from frontend
     */

    protected abstract function _processJobFromFrontend(KJob $job);

    /*
     * This is called in a loop continously on frontend
     */

    protected abstract function _frontendRoutine(KJob $job);

    /*
     * This is called in a loop continously on backend
     */

    protected abstract function _backendRoutine(KJob $job);

    protected function _mainThread() {

        while (1) {
            if ($this->_position == 'frontend') {
                $this->_frontendRoutine();
            } else if ($this->_position == 'backend') {
                $this->_backendRoutine();
            }
        }
    }

    public static function getWorker($worker_type, $position) {
        $className = $worker_type . 'KWorker';
        return new $className($position);
    }

    public function processJob(KJob $job) {
        if ($job->getSender() == 'frontend') {
            return $this->processJobFromFrontend($job);
        } else if ($job->getSender() == 'backend') {
            return $this->processJobFromBackend($job);
        }
    }

    public function fillAddressPool() {
        $criteria = new CDbCriteria;
        $criteria->compare('wallet', null);
        $criteria->compare('currency', $this->_currency);
        $availableAddressCount = Address::model()->count($criteria);
        
        if($availableAddressCount < MIN_NUM_OF_AVAILABLE_ADDRESSES) {
            
            // TODO: send new address request  
            $job = $this->_createNewJob(NEW_ADDRESS_REQUEST,P_BACKEND);
            $this->_sendJob($job);
        }
    }
    
    private function _createNewJob($definition, $receiver) {
        return new KJob($this->_currency, $this->$position, $receiver, $definition);
    }

    private function _sendJob(KJob $job){
        // TODO Send to queue
    }
}
?>
