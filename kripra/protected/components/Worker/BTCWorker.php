<?php


class BTCWorker extends KWorker{


    public function __construct() {
        $this->_currency = 'BTC';
        parent::__construct();
    }

    protected function _backendRoutine(\KJob $job) {
        
    }

    protected function _frontendRoutine(\KJob $job) {
        
    }

    protected function _processJobFromBackend(\KJob $job) {
        
    }

    protected function _processJobFromFrontend(\KJob $job) {
        
    }    
}
?>
